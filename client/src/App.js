import React, { Component } from 'react';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { createUploadLink } from 'apollo-upload-client';

//components
import BookList from './components/BookList';
import AddBook from './components/AddBook';
import { getBooksQuery } from './queries/queries';
import Login from './Login';
import HelloSign from 'hellosign-embedded';

const cache = new InMemoryCache();

const client1 = new HelloSign({
  clientId: 'e8bef94dd5a2e23cf4e32bfd9de4fd4a',
});

client1.open(
  'https://app.hellosign.com/editor/embeddedSign?signature_id=4615ad0b6e041895cd76442668f100f2&token=8ce557b1543898f7137a1668aebc1760',
  {
    allowCancel: true,
    debug: true,
    skipDomainVerification: true,
  }
);

//apollo client setup
const client = new ApolloClient({
  link: createUploadLink({
    uri: 'http://localhost:4000/graphql',
  }),
  cache,
  resolvers: {
    Book: {
      selected: book => book.selected || false,
    },

    Mutation: {
      getSelectBook: (_root, args, { cache, getCacheKey }) => {
        let { books } = cache.readQuery({ query: getBooksQuery });
        const selectedBook = books.map(book => book.selected);
        return selectedBook;
      },
      toggleBook: (_root, variables, { cache, getCacheKey }) => {
        // Fragment
        // const id = getCacheKey({ id: variables.id, __typename: 'Book' });
        // const fragment = gql`
        //   fragment bookToSelect on Book {
        //     selected
        //   }
        // `;

        // const book = cache.readFragment({ fragment, id });
        // const data = { ...book, selected: !book.selected };
        // cache.writeFragment({ fragment, id, data });

        // Query
        let { books } = cache.readQuery({ query: getBooksQuery });

        books = books.map(book => {
          if (book.id === variables.id) {
            return { ...book, selected: true };
          }
          return { ...book, selected: false };
        });

        cache.writeQuery({
          query: getBooksQuery,
          data: {
            books,
          },
        });
        return null;
      },

      setActiveBook: (_root, variables, { cache, getCacheKey }) => {
        console.log('START:', _root, variables, cache, getCacheKey);
      },
    },
  },
});

const defaultState = {
  name: '',
  genre: '',
  authorId: '',
};

export const AppContext = React.createContext(defaultState);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...defaultState,
      updateContext: this.updateContext,
      updateAllContext: this.updateAllContext,
    };
  }

  updateContext = ({ key, value }) => {
    this.setState({
      [key]: value,
    });
  };

  updateAllContext = obj => {
    this.setState(obj);
  };

  render() {
    return (
      <ApolloProvider client={client}>
        <AppContext.Provider value={this.state}>
          <BrowserRouter>
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/home" component={Main} />
              <Redirect to="/login" />
            </Switch>
          </BrowserRouter>
        </AppContext.Provider>
      </ApolloProvider>
    );
  }
}

const Main = () => (
  <div id="main">
    <h1>Ninja Reading List</h1>
    <BookList />
    <AddBook />
  </div>
);

export default App;
