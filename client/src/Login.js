import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';

export const UPLOAD_FILE = gql`
  mutation uploadFile($file: Upload!) {
    uploadFile(file: $file) {
      filename
      mimetype
      encoding
    }
  }
`;

export const MULTIPLE_UPLOAD_FILE = gql`
  mutation multipleUploadFile($files: [Upload]!) {
    multipleUploadFile(files: $files) {
      filename
      mimetype
      encoding
    }
  }
`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Mutation mutation={MULTIPLE_UPLOAD_FILE}>
        {(multipleUploadFile, { data, error }) => {
          console.log('DATA:', data);
          console.log('ERROR:', error);

          return (
            <input
              type="file"
              multiple
              required
              onChange={({ target: { validity, files } }) => {
                console.log(files);
                validity.valid && multipleUploadFile({ variables: { files } });
              }}
            />
          );
        }}
      </Mutation>
    );
  }
}

export default Login;
