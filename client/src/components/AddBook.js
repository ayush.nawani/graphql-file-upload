import React, { Component, PureComponent } from 'react';
import {
  // graphql, compose,
  Query,
  Mutation,
} from 'react-apollo';
import {
  getAuthorsQuery,
  addBookMutation,
  getBooksQuery,
} from '../queries/queries';
import { adopt } from 'react-adopt';
import { AppContext } from '../App';

class AddBook extends PureComponent {
  state = {
    name: '',
    genre: '',
    authorId: '',
  };

  displayAuthors({ data, loading, authorId }) {
    if (loading) {
      return <option disabled>Loading Authors...</option>;
    }

    return data.authors.map(author => {
      return (
        <option key={author.id} value={author.id}>
          {author.name}
        </option>
      );
    });
  }

  submitForm(addBookMutation, { genre, authorId, name }) {
    // let { name, genre, authorId } = this.state;

    // e.preventDefault();
    addBookMutation({
      variables: { name, genre, authorId },
      refetchQueries: [
        {
          query: getBooksQuery,
        },
      ],
    });
  }

  render() {
    const Compose = adopt({
      bookMutation: ({ render }) => (
        <Mutation mutation={addBookMutation}>
          {(mutation, result) => render({ mutation, result })}
        </Mutation>
      ),
      authorQuery: ({ render }) => (
        <Query query={getAuthorsQuery}>{render}</Query>
      ),

      appContext: ({ render }) => (
        <AppContext.Consumer>{render}</AppContext.Consumer>
      ),
    });

    return (
      <Compose>
        {({
          appContext: { updateContext, genre, authorId, name },
          bookMutation: { mutation, result },
          authorQuery: { data, loading, error },
        }) => (
          <form
            id="add-book"
            onSubmit={e => {
              e.preventDefault();
              this.submitForm(mutation, { genre, authorId, name });
            }}
          >
            <div className="field">
              <span className="field-label">
                <label>Book name:</label>
              </span>
              <input
                type="text"
                value={name}
                onChange={e =>
                  updateContext({ key: 'name', value: e.target.value })
                }
              />
            </div>
            <div className="field">
              <span className="field-label">
                <label>Genre:</label>
              </span>
              <input
                type="text"
                value={genre}
                onChange={e =>
                  updateContext({ key: 'genre', value: e.target.value })
                }
              />
            </div>
            <div className="field">
              <span className="field-label">
                <label>Author:</label>
              </span>
              <select
                value={authorId}
                onChange={e =>
                  updateContext({ key: 'authorId', value: e.target.value })
                }
              >
                <option>Select Author</option>
                {this.displayAuthors({ data, loading, error, authorId })}
              </select>
            </div>
            )}
            <button>+</button>
          </form>
        )}
      </Compose>
    );
  }
}

export default AddBook;

// export default compose(
//   graphql(getAuthorsQuery, { name: 'getAuthorsQuery' }),
//   graphql(addBookMutation, { name: 'addBookMutation' })
// )(AddBook);
