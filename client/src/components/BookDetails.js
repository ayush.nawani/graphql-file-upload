import React, { Component } from 'react';
import {
  //  graphql,
  Query,
  Mutation,
} from 'react-apollo';

import { adopt } from 'react-adopt';
import { getBookQuery } from '../queries/queries';
import { AppContext } from '../App';

class BookDetails extends Component {
  state = {};

  displayBookDetails(getBookQuery, { updateAllContext }) {
    let { data } = getBookQuery;
    let { book } = data;
    if (getBookQuery.loading) {
      return <div>Loading Book Detail...</div>;
    }

    if (book) {
      return (
        <div>
          <h2>{book.name}</h2>
          <button
            onClick={() => {
              updateAllContext({
                name: book.name,
                genre: book.genre,
                authorId: book.author.id,
              });
            }}
          >
            edit
          </button>
          <p>{book.genre}</p>
          <p>{book.author.name}</p>
          <p>All books by this author</p>
          <div className="other-books">
            {book.author.books.map(item => {
              return <li key={item.id}> {item.name}</li>;
            })}
          </div>
        </div>
      );
    }

    return <div>No book selected...</div>;
  }

  render() {
    const Composed = adopt({
      getBookQuery: ({ render }) => (
        <Query query={getBookQuery} variables={{ id: this.props.bookId }}>
          {render}
        </Query>
      ),
      appContext: ({ render }) => (
        <AppContext.Consumer>{render}</AppContext.Consumer>
      ),
    });

    return (
      <Composed>
        {({ getBookQuery, appContext }) => {
          return (
            <div id="book-details">
              {this.displayBookDetails(getBookQuery, appContext)}
            </div>
          );
        }}
      </Composed>
    );
  }
}

export default BookDetails;

// export default graphql(getBookQuery, {
//   options: props => {
//     return {
//       variables: {
//         id: props.bookId,
//       },
//     };
//   },
// })(BookDetails);
