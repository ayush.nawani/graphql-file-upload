import React, { Component } from 'react';
import {
  // graphql,
  Query,
  Mutation,
} from 'react-apollo';
import { getBooksQuery, selectBook } from '../queries/queries';
import { adopt } from 'react-adopt';
import BookDetails from './BookDetails';
import classNames from 'classnames';

class BookList extends Component {
  state = {
    selected: null,
  };

  displayBooks(books, toggleBook) {
    let { data } = books;
    if (books.loading) {
      return <div>Loading Books...</div>;
    }

    return data.books.map(book => {
      return (
        <li
          key={book.id}
          className={classNames({
            'book-seleted': book.selected,
          })}
          onClick={e => {
            toggleBook({
              variables: { id: book.id },
              // refetchQueries: [{ query: getBooksQuery }],
            });
            this.setState({ selected: book.id });
          }}
        >
          {book.name}
        </li>
      );
    });
  }

  render() {
    const Compose = adopt({
      books: ({ render }) => <Query query={getBooksQuery}>{render}</Query>,
      toggleBook: ({ render }) => (
        <Mutation mutation={selectBook}>{render}</Mutation>
      ),
    });

    return (
      <Compose>
        {({ books, toggleBook }) => {
          return (
            <div id="main">
              <ul id="book-list">{this.displayBooks(books, toggleBook)}</ul>
              <BookDetails bookId={this.state.selected} />
            </div>
          );
        }}
      </Compose>
    );
  }
}

export default BookList;
// export default graphql(getBooksQuery)(BookList);
