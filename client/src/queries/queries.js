import gql from 'graphql-tag';

const setActiveBook = gql`
  mutation SetActiveBook($id: ID) {
    setActiveBook(id: $id) @client
  }
`;

const selectBook = gql`
  mutation($id: ID) {
    toggleBook(id: $id) @client
  }
`;

const getActiveBook = gql`
  {
    activeBook
  }
`;

const getBooksQuery = gql`
  {
    books {
      name
      id
      selected @client
    }
  }
`;

const getBookQuery = gql`
  query($id: ID) {
    book(id: $id) {
      id
      name
      genre
      # selected @client
      author {
        id
        name
        age
        books {
          name
          id
        }
      }
    }
  }
`;

const getAuthorsQuery = gql`
  {
    authors {
      name
      id
    }
  }
`;

const addBookMutation = gql`
  mutation($name: String!, $genre: String!, $authorId: ID!) {
    addBook(name: $name, genre: $genre, authorId: $authorId) {
      name
      id
    }
  }
`;

export {
  getAuthorsQuery,
  getBooksQuery,
  addBookMutation,
  getBookQuery,
  getActiveBook,
  setActiveBook,
  selectBook,
};
