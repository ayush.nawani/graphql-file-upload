import React, { useState, useEffect } from 'react';
import './App.css';

export default function App() {
  const [counter, setCounter] = useState(0);
  const [visible, setVisible] = useState(true);
  const [width, setWidth] = useState(window.innerWidth);

  const handleWidth = () => setWidth(window.innerWidth);
  const handleVisible = () => setVisible(!visible);
  const handleCounter = () => setCounter(counter + 1);

  useEffect(() => {
    if (visible) {
      console.log('Init');
      window.addEventListener('resize', handleWidth);
    }
    return () => {
      console.log('Flush');
      window.removeEventListener('resize', handleWidth);
    };
  }, [visible]);

  return (
    <div>
      <button
        onClick={() => {
          setCounter(counter - 1);
        }}
      >
        -
      </button>
      <div>{counter}</div>
      {visible && <div>Width {width}</div>}
      <button onClick={handleVisible}>Toggle Visibility</button>
      <button onClick={handleCounter}>+</button>
    </div>
  );
}
