// const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const typeDefs = require('./schema/typeDefs');
const { ApolloServer } = require('apollo-server');
const mongoose = require('mongoose');
const cors = require('cors');
// const fetch = require('node-fetch');
const docusign = require('docusign-esign');
const apiClient = new docusign.ApiClient();

const integratorKey = '56c9b66c-22b4-461d-87c8-3bb354f5128d'; // An IK for a non-mobile docusign account
const clientSecret = 'ea5c7f7c-53a9-4239-b8d7-f93a560f2c2b';
const redirectUri = 'http://localhost:3000/auth'; // This needs to be registered with the integrator key in your admin account
const basePath = 'https://demo.docusign.net/restapi';

const responseType = apiClient.OAuth.ResponseType.CODE; // Response type of code, to be used for the Auth code grant
const scopes = [apiClient.OAuth.Scope.EXTENDED];
const randomState = '*^.$DGj*)+}Jk'; // after successful login you should compare the value of URI decoded "state" query param with the one created here. They should match

// var counter = 0;

// setInterval(() => {
//   Array(1000)
//     .fill(0)
//     .forEach((item, index) => {
//       counter += 3;
//       console.log('COUNTER', counter);
//       fetch('http://192.168.100.14:5000?user=client1')
//         .then(res => {
//           return res.json();
//         })
//         .then(result => {
//           console.log('CLIENT1', result);
//         });
//       fetch('http://192.168.100.14:5000?user=client2')
//         .then(res => {
//           return res.json();
//         })
//         .then(result => {
//           console.log('CLIENT2', result);
//         });
//       fetch('http://192.168.100.14:5000?user=client3')
//         .then(res => {
//           return res.json();
//         })
//         .then(result => {
//           // console.log("CLIENT3", result);
//         });
//     });
// }, 1000);

// const app = express();

// allow cross-origin request
// app.use(cors());

//connect to mlab database
mongoose.connect('mongodb://admin:admin123@ds349045.mlab.com:49045/gql-ninja', {
  useNewUrlParser: true,
});
mongoose.connection.once('open', () => {
  console.log('connected to database');
});

const server = new ApolloServer({
  typeDefs,
  resolvers: schema,
  formatError: error => {
    // console.log(error);

    // Or, you can delete the exception information
    // let err = JSON.stringify(error);
    // err = JSON.parse(err);
    let err = { ...error };
    delete err.extensions.exception;
    delete err.locations;
    delete err.path;
    console.log('NEW', err);
    return err;
  },
});

// app.use(
//   '/graphql',
//   graphqlHTTP({
//     schema,
//     graphiql: true,
//   })
// );

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
