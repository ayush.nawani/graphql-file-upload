const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookSchema = new Schema({
  name: String,
  genre: String,
  authorId: String,
});

// models means collection in mongodb
module.exports = mongoose.model('Book', bookSchema);
