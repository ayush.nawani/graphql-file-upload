const graphql = require('graphql');
const _ = require('lodash');
const Book = require('../models/book');
const Author = require('../models/author');
const { ApolloError } = require('apollo-server');
const fs = require('fs');

// get all graphql types
const {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLID,
  GraphQLString,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull,
} = graphql;

// dummy data

// var books = [
//   { name: 'Name of the Winds', genre: 'Anime', id: '1', authorId: '1' },
//   { name: 'Lala Land', genre: 'Fiction', id: '2', authorId: '2' },
//   { name: 'Pirates of the Space', genre: 'Scifi', id: '3', authorId: '3' },
//   { name: 'Gravity', genre: 'Scifi', id: '4', authorId: '3' },
//   { name: 'Up', genre: 'Animation', id: '5', authorId: '2' },
// ];

// var authors = [
//   { name: 'Peter Parker', age: 30, id: '1' },
//   { name: 'Robbie Williams', age: 44, id: '2' },
//   { name: 'Patrick Harrison', age: 39, id: '3' },
// ];

// create book schema
const BookType = new GraphQLObjectType({
  name: 'Book',
  // field is assigned function, we will discuss later
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    genre: { type: GraphQLString },
    author: {
      type: AuthorType,
      resolve(parent, args) {
        // return _.find(authors, { id: parent.authorId });
        return Author.findById(parent.authorId);
      },
    },
  }),
});

const AuthorType = new GraphQLObjectType({
  name: 'Author',
  fields: {
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    age: { type: GraphQLInt },
    books: {
      type: new GraphQLList(BookType),
      resolve(parent, args) {
        // return _.filter(books, { authorId: parent.id });
        return Book.find({ authorId: parent.id });
      },
    },
  },
});

// base query which will handle frontend requests
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    book: {
      type: BookType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        // code to get data from db/other source
        // return _.find(books, { id: args.id });
        return Book.findById(args.id);
      },
    },

    author: {
      type: AuthorType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        // code to get data from db/other source
        // return _.find(authors, { id: args.id });
        return Author.findById(args.id);
      },
    },

    books: {
      type: new GraphQLList(BookType),
      resolve(parent, args) {
        // return books;
        return Book.find({});
      },
    },

    authors: {
      type: new GraphQLList(AuthorType),
      resolve(parent, args) {
        // return authors;
        return Author.find({});
      },
    },
  },
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addAuthor: {
      type: AuthorType,
      args: { name: { type: GraphQLString }, age: { type: GraphQLInt } },
      resolve(parent, args) {
        let author = new Author({
          name: new GraphQLNonNull(args.name),
          age: new GraphQLNonNull(args.age),
        });

        return author.save();
      },
    },

    addBook: {
      type: BookType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        genre: { type: new GraphQLNonNull(GraphQLString) },
        authorId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve(parent, args) {
        let book = new Book({
          name: args.name,
          genre: args.genre,
          authorId: args.authorId,
        });

        return book.save();
      },
    },
  },
});

const storeFS = ({ stream, filename, mimetype, encoding }) => {
  const path = `${process.cwd() + '/uploads'}/${filename}`;
  return new Promise((resolve, reject) =>
    stream
      .on('error', error => {
        if (stream.truncated)
          // Delete the truncated file.
          fs.unlinkSync(path);
        reject(error);
      })
      .pipe(fs.createWriteStream(path))
      .on('error', error => reject(error))
      .on('finish', () => resolve({ stream, filename, mimetype, encoding }))
  );
};

const processUpload = async upload => {
  const { createReadStream, filename, mimetype, encoding } = await upload;
  const stream = createReadStream();
  const res = await storeFS({ stream, filename, mimetype, encoding });
  return { filename, mimetype, encoding };
};

const data = {
  Query: {
    books: (root, args) => {
      return Book.find({});
    },

    authors: (root, args) => {
      return Author.find({});
    },

    uploads: () => {
      // Return the record of files uploaded from your DB or API or filesystem.
    },
  },
  Mutation: {
    async multipleUploadFile(parent, { files }) {
      console.log(files);
      const data = await Promise.all(files.map(processUpload));
      console.log(data);
      console.log(data);
      // if (reject.length)
      //   reject.forEach(({ name, message }) =>
      //     // eslint-disable-next-line no-console
      //     console.error(`${name}: ${message}`)
      //   );

      return data;
    },

    async uploadFile(parent, { file }) {
      const { stream, filename, mimetype, encoding } = await file;

      // 1. Validate file metadata.

      // 2. Stream file contents into cloud storage:
      // https://nodejs.org/api/stream.html

      // 3. Record the file upload in your DB.
      // const id = await recordFile( … )
      const uploadDir = process.cwd() + '/uploads';
      const path = `${uploadDir}/${filename}`;
      return new Promise((resolve, reject) => {
        stream
          .on('error', error => {
            if (stream.truncated)
              // delete the truncated file
              fs.unlinkSync(path);
            reject(error);
          })
          .pipe(fs.createWriteStream(path))
          .on('error', error => {
            console.log('ERR', error);
            reject({});
          })
          .on('finish', () => {
            console.log('DONE');
            reject(new ApolloError('Eerror', 401));
            var data = { filename, mimetype, encoding };
            console.log('DATA:', data);
            // resolve(data);
          });
      });

      // return { filename, mimetype, encoding };
    },
    addAuthor: (_, { name, age }) => {
      let author = new Author({
        name,
        age,
      });

      return author.save();
    },

    addBook: (_, { name, genre, authorId }) => {
      let book = new Book({
        name,
        genre,
        authorId,
      });

      return book.save();
    },
  },
};

module.exports = data;
