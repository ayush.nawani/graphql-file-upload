const { gql } = require('apollo-server');

const typeDefs = gql`
  type Query {
    book: Book
    author: Author
    books: [Book]
    authors: [Author]
    uploads: [File]
  }

  type Mutation {
    addAuthor(name: String!, age: Int!): Author
    addBook(name: String!, genre: String!, authorId: ID!): Book
    uploadFile(file: Upload!): File!
    multipleUploadFile(file: [Upload]!): [File]!
  }

  type File {
    filename: String
    mimetype: String
    encoding: String
  }

  type Book {
    id: ID!
    name: String
    genre: String
    author: Author
  }

  type Author {
    id: ID!
    name: String
    age: Int
    books: [Book]
  }
`;

module.exports = typeDefs;
